<section class="partners">
    <div class="partners__border border">
        <div class="partners__container">
           <div id="slideshow2">
                <div class="slick slider-2">
                    <div  class="box"><div class="partners__icon"></div></div>
                    <div class="box"><div class="partners__icon partners__icon--one"></div></div>
                    <div  class="box"><div class="partners__icon"></div></div>
                    <div  class="box"><div class="partners__icon partners__icon--two"></div></div>
                    <div  class="box"><div class="partners__icon"></div></div>
                    <div  class="box"><div class="partners__icon partners__icon--three"></div></div>
                    <div  class="box"><div class="partners__icon"></div></div>
                    <div  class="box"><div class="partners__icon partners__icon--four"></div></div>
                    <div  class="box"><div class="partners__icon"></div></div>
                    <div  class="box"><div class="partners__icon partners__icon--five"></div></div>
                </div>
            </div>
        </div>
    </div>
</section>