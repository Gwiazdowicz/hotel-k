<section class="projetcts">
    <div class="projetcts__border border">
        <div class="projetcts__container">
        <div class="projetcts__box-top">
            <p class="projetcts__text  a-text a-text--blue a-text--small">lutpat laoreet fermentum in dolor</p>
            <h2 class="projetcts__title  a-title a-title--blue a-title--small">Nasze realizacje</h2>
        </div>
        <div id="slideproject">
            <div class="slick slider-3">
                 <div class="projetcts__tile">
                    <a href="http://galacticshop.cba.pl/"><div class="projetcts__tile_img projetcts__tile_img--one"></div></a>
                    <div class="projetcts__info">
                        <h3 class="projetcts__tile-title">Sansara</h3>
                        <p class="projetcts__tile-discription a-article  a-article--realizaction">Fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec alesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum.</p>
                        <a href="http://galacticshop.cba.pl/" class="projetcts__tile-webside">www.galacticshop.cba.pl</a>
                        <div class="projetcts__tile-rwd">
                            <div class="projetcts__tile-rwd-img"></div>
                            <p class="projetcts__tile-rwd-text a-article">Responsive Web Design</p>
                        </div>
                    </div>
                </div>
                <div class="projetcts__tile">
                    <a href="http://galacticshop-2.cba.pl/"><div class="projetcts__tile_img projetcts__tile_img--two"></div></a>
                    <div class="projetcts__info">
                        <h3 class="projetcts__tile-title">K Hotel</h3>
                        <p class="projetcts__tile-discription a-article  a-article--realizaction">Fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec alesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum.</p>
                        <a href="http://galacticshop-2.cba.pl/" class="projetcts__tile-webside">galacticshop-2.cba.pl</a>
                        <div class="projetcts__tile-rwd">
                            <div class="projetcts__tile-rwd-img"></div>
                            <p class="projetcts__tile-rwd-text a-article">Responsive Web Design</p>
                        </div>
                    </div>
                </div>
                <div class="projetcts__tile">
                    <a href="http://galacticshop-3.cba.pl/"><div class="projetcts__tile_img projetcts__tile_img--three"></div></a>
                    <div class="projetcts__info">
                        <h3 class="projetcts__tile-title">Apartamenty Mazury</h3>
                        <p class="projetcts__tile-discription a-article  a-article--realizaction">Fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec alesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum.</p>
                        <a href="#!" class="projetcts__tile-webside">www.galacticshop-3.cba.pl</a>
                        <div class="projetcts__tile-rwd">
                            <div class="projetcts__tile-rwd-img"></div>
                            <p class="projetcts__tile-rwd-text a-article">Responsive Web Design</p>
                        </div>
                    </div>
                </div>
                <div class="projetcts__tile">
                <a href="http://galacticshop-3.cba.pl/"><div class="projetcts__tile_img projetcts__tile_img--four"></div></a>
                    <div class="projetcts__info">
                        <h3 class="projetcts__tile-title">Wannabuy</h3>
                        <p class="projetcts__tile-discription a-article  a-article--realizaction">Fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec alesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum.</p>
                        <a href="#!" class="projetcts__tile-webside">www.galacticshop-4.cba.pl</a>
                        <div class="projetcts__tile-rwd">
                            <div class="projetcts__tile-rwd-img"></div>
                            <p class="projetcts__tile-rwd-text a-article">Responsive Web Design</p>
                        </div>
                    </div>
                </div>
                <div class="projetcts__tile">
                    <a href="http://galacticshop-3.cba.pl/"><div class="projetcts__tile_img projetcts__tile_img--five"></div></a>
                    <div class="projetcts__info">
                        <h3 class="projetcts__tile-title">MamPlan</h3>
                        <p class="projetcts__tile-discription a-article  a-article--realizaction">Fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec alesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum.</p>
                        <a href="#!" class="projetcts__tile-webside">www.galacticshop-5.cba.pl</a>
                        <div class="projetcts__tile-rwd">
                            <div class="projetcts__tile-rwd-img"></div>
                            <p class="projetcts__tile-rwd-text a-article">Responsive Web Design</p>
                        </div>
                    </div>
                </div>
                <div class="projetcts__tile">
                    <a href="http://galacticshop-3.cba.pl/"><div class="projetcts__tile_img"></div></a>
                    <div class="projetcts__info">
                        <h3 class="projetcts__tile-title"> Zakopane</h3>
                        <p class="projetcts__tile-discription a-article  a-article--realizaction">Fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec alesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum.</p>
                        <a href="#!" class="projetcts__tile-webside">www.galacticshop-6.cba.pl</a>
                        <div class="projetcts__tile-rwd">
                            <div class="projetcts__tile-rwd-img"></div>
                            <p class="projetcts__tile-rwd-text a-article">Responsive Web Design</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>
