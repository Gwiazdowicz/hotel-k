<section class="clients">
    <div class="clients__border border">
        <div class="clients__container">
            <h2 class="clients__title a-title a-title--small"> Zadowoleni klienci</h2>
                 <div id="slideshow">
                    <div class="slick">
                    <div class="clients__tile">
                        <blockquote class="blockquote-box">
                            <div>
                                <p class="clients__text a-article  a-article--white">Dzięki KWHotel mamy obłożenie przez cały rok! Nie muszę się martwić o los naszego hotelu :) Ut molestie a, ultricies porta urna hasellus ferm entum in, dolor. Pellentesque facilisis ulla imper diet sit amet magna. Marek VipHotel, Kraków .</p>
                                <footer class="clients__namebig">Jan</footer><br>
                                <cite class="clients__citybig">KingSize, Warszawa</cite>
                            </div>
                            
                            <div class="clients__box-person">
                                <div class="clients__box-img clients__box-img--one"></div>
                                <div>
                                    <footer class="clients__name">Jan</footer><br>
                                    <cite class="clients__city">KingSize, Warszawa</cite>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                    <div class="clients__tile">
                        <blockquote class="blockquote-box">
                            <div>
                                <p class="clients__text a-article  a-article--white">Dzięki KWHotel mamy obłożenie przez cały rok! Nie muszę się martwić o los naszego hotelu :) Ut molestie a, ultricies porta urna hasellus ferm entum in, dolor. Pellentesque facilisis ulla imper diet sit amet magna. Marek VipHotel, Kraków .</p>
                                <footer class="clients__namebig">Marek</footer><br>
                                <cite class="clients__citybig">VipHotel, Kraków</cite>
                            </div>
                            
                            <div class="clients__box-person">
                                <div class="clients__box-img"></div>
                                <div>
                                    <footer class="clients__name">Marek</footer><br>
                                    <cite class="clients__city">VipHotel, Kraków</cite>
                                </div>
                            </div>
                        </blockquote>
                    </div>

                    <div class="clients__tile">
                        <blockquote class="blockquote-box">
                            <div>
                                <p class="clients__text a-article  a-article--white">Dzięki KWHotel mamy obłożenie przez cały rok! Nie muszę się martwić o los naszego hotelu :) Ut molestie a, ultricies porta urna hasellus ferm entum in, dolor. Pellentesque facilisis ulla imper diet sit amet magna. Marek VipHotel, Kraków .</p>
                                <footer class="clients__namebig">Ryszard</footer><br>
                                <cite class="clients__citybig">Star Home,  Łódź</cite>
                            </div>
                            
                            <div class="clients__box-person">
                                <div class="clients__box-img clients__box-img--three"></div>
                                <div>
                                    <footer class="clients__name">Ryszard</footer><br>
                                    <cite class="clients__city">Star Home, Łódź</cite>
                                </div>
                            </div>
                        </blockquote>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


    

