<section class="hero">
        <div class="hero__content">
            <div class="hero__border borders">
                <div class="hero__container container">
                    <div class="hero__top-box">
                        <div class="hero__contact-box">
                            <a href="tel:620-103-200" class="hero__contact-tel">620 103 200</a>
                            <a href="!#" class="hero__contact-btn">
                                <p  class="hero__contact-btn-text">Poznaj ofertę</p>
                                <div  class="hero__contact-btn-arrow"></div>
                            </a>
                        </div>
                        <a href="#!"><div class="hero__logo"></div></a>
                    </div>
                    <div class="hero__bottom-box">
                        <div class="hero__text-box">
                            <p class="hero__text a-text"> laoreet orem ipsum fermentum in dolor</p>
                            <h1 class="hero__title a-title"> Nadszedł czas na zwiększenie liczby gości</h1>
                            <p class="hero__article">Luctus et interdum adipiscing wisi liquam n massa ac turpis faucibus consectetu erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum. </p>
                            <div class="hero__play"></div>
                        </div>
                        <div class="hero__form-box">
                            <form>
                                <input type="text" name="firstname">
                                <input type="text" name="lastname">
                                <input  class="hero__btn" type="submit" value="Chcę 5 pomysłów na rozwój">
                            </form> 
                        </div>
                    </div>
                </div>
            </div>    
        </div>
</section>



    

