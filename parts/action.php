<section class="action">
    <div class="action__block">
        <div class="action__border borders">
            <div class="action__container container">
                <div class="action__box-one">
                    <p class="action__text a-text a-text--small">lutpat laoreet fermentum in dolor</p>
                    <h2 class="action__title a-title a-title--small">Jak to działa?</h2>
                    <p class="action__discription a-article a-article--white">Nullam wisi ultricies a, gravida vitae, dapibus risus ante sodales lectus blandit eu, tempor diam pede cursus vitae, ultricies eu, faucibus quis, porttitor eros cursus lectus, pellentesque eget, bibendum a, gravida ullamcorper quam. Nullam viverra consectetuer. </p>
                </div>
                <div class="action__box-two">
                    <div class="action__tile">
                        <div class="action__tile-top">
                            <p class="action__tile-num a-num">01</p>
                            <p class="action__tile-title a-tile-title">Lorem ipsum dolor</p>
                        </div>
                        <p class="action__tile-subtitle">Pellentesque facilisis diet sit amet magna. Vestibulum dapibus malesuada fames turpis velit, rhoncus eu, luctus et interdum adipiscing </p>
                    </div>
                    <div class="action__tile">
                        <div class="action__tile-top">
                            <p class="action__tile-num a-num">02</p>
                            <p class="action__tile-title a-tile-title">Lorem ipsum dolor</p>
                        </div>
                        <p class="action__tile-subtitle">Pellentesque facilisis diet sit amet magna. Vestibulum dapibus malesuada fames turpis velit, rhoncus eu, luctus et interdum adipiscing </p>
                    </div>
                    <div class="action__tile">
                        <div class="action__tile-top">
                            <p class="action__tile-num a-num">03</p>
                            <p class="action__tile-title a-tile-title">Lorem ipsum dolor</p>
                        </div>
                        <p class="action__tile-subtitle">Pellentesque facilisis diet sit amet magna. Vestibulum dapibus malesuada fames turpis velit, rhoncus eu, luctus et interdum adipiscing </p>
                    </div>
                    <div class="action__tile">
                        <div class="action__tile-top">
                            <p class="action__tile-num a-num">04</p>
                            <p class="action__tile-title a-tile-title">Lorem ipsum dolor</p>
                        </div>
                        <p class="action__tile-subtitle">Pellentesque facilisis diet sit amet magna. Vestibulum dapibus malesuada fames turpis velit, rhoncus eu, luctus et interdum adipiscing </p>
                    </div>
                </div>
            </div>
        </div>  
    </div>  
</section>
