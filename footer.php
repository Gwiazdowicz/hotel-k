<section class="footer">
    <div class="footer__border border">
        <div class="footer__container">
            <div class="footer__box-top">
                <h2 class="footer__title a-title a-title--small">nie zwlekaj!</h2>
                <p class="footer__text a-article a-article--white"> Nullam wisi ultricies a, gravida vitae, dapibus risus ante sodales lectus blandit eu, tempor diam pede cursus vitae, ultricies eu, cursus lec ullam viverra consectetuer.</p>
            </div>
            <div class="footer__box-form">
                <form>
                    <input type="text" name="firstname">
                    <input type="text" name="lastname">
                    <input  class="hero__btn" type="submit" value="Chcę 5 pomysłów na rozwój">
                </form> 
            </div>
            <div class="footer__box-icons">
                <a href ="#!" class="footer__icon-insta  "></a>
                <a href ="#!" class="footer__icon-fb "></a>
                <a href ="#!" class="footer__icon-tweet "></a>
            </div>
            <div class="footer__box-icons-big">
                <div class="footer__box-icon-text">
                    <a href ="https://www.instagram.com/?hl=pl" class="footer__icon-insta"></a>
                    <a href="https://www.instagram.com/?hl=pl"><p class="footer__lasttext">Instagram</p></a>
                </div>
                <div class="footer__box-icon-text">
                    <a href ="https://pl-pl.facebook.com/" class="footer__icon-fb "></a>
                    <a href="https://pl-pl.facebook.com/"><p class="footer__lasttext">Facebook</p></a>
                </div>
                <div class="footer__box-icon-text">
                    <a href ="https://twitter.com/explore" class="footer__icon-tweet "></a>
                    <a href="https://twitter.com/explore"><p class="footer__lasttext">Tweeter</p></a>
                </div>
            </div>
        </div>
    </div>
</section>
</main>
</body>
</html>
