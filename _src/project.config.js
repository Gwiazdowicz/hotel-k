module.exports = {
  // provide context for webpack
  // this should point to where your assets for development are
  context: 'dev-assets',

  // where should output files be placed
  outputFolder: '../assets',

  publicFolder: 'assets',

  // main entry point for all the stuff
  entry: {
    scripts: './app-entry.js'
  },

  devtool: 'cheap-module-eval-source-map',

  proxyTarget: 'http://localhost/',

  // where the php files are
  watch: ['../**/*.php']
};
