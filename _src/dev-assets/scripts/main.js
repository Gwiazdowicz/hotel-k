console.log('main.js imported');

$('#slideshow .slick').slick({
    slidesToShow: 1,
    dots: true,
    arrows: false,
    autoplay: true,
    autoSpeed: 8000,
    speed: 3000,
  });


$('#slideshow2 .slick').slick({
    slidesToShow: 4,
    dots: false,
    arrows: false,
    autoplay: true,
    autoSpeed: 8000,
    speed: 3000,
    mobileFirst:true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 8,
          slidesToScroll: 4,
          centerPadding: '40px'
        }
      },
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 9,
          focusOnSelect: true
        }
      }
    ]
  });

  $('#slideproject .slick').slick({
    slidesToShow: 1,
    dots: true,
    arrows: false,
    autoplay: true,
    autoSpeed: 8000,
    speed: 3000,
    centerMode: true,
    centerPadding: '30px',
    mobileFirst:true,
    responsive: [

      {
        breakpoint: 768,
        settings: {
        slidesToShow: 2,
        centerMode: true,
        centerPadding: '30px',
        }
      
    },
      {
          breakpoint: 1440,
          settings: "unslick"
          
      }
    ]
  }); 
