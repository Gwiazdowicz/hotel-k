const path = require('path');
const webpack = require('webpack');

const MiniCssExtractWebpackPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const NonJsEntryCleanupPlugin = require('./non-js-entry-cleanup-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');

const {
  context,
  entry,
  devtool,
  outputFolder,
  publicFolder
} = require('./../project.config');

const HMR = require('./hmr');
const getPublicPath = require('./publicPath');

module.exports = options => {
  const { dev, pretty } = options;

  const hmr = HMR.getClient();

  return {
    mode: dev || pretty ? 'development' : 'production',
    devtool: dev || pretty ? devtool : false,
    context: path.resolve(context),
    entry: {
      'js/main': dev ? [hmr, entry.scripts] : entry.scripts
    },
    output: {
      path: path.resolve(outputFolder),
      publicPath: dev ? getPublicPath(publicFolder) : '../',
      filename: dev || pretty ? 'js/main.js' : 'js/main.min.js'
    },
    resolve: {
      alias: {
        img: path.resolve(context, 'img'),
        fonts: path.resolve(context, 'fonts'),
        video: path.resolve(context, 'video'),
        vendor: path.resolve(context, 'vendor')
      }
    },
    externals: {
      jquery: 'jQuery'
    },
    optimization: {
      minimizer: [
        ...(dev || pretty
          ? []
          : [
              new TerserPlugin({
                terserOptions: {
                  compress: {
                    drop_console: true
                  }
                }
              })
            ])
      ]
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)\/(?!(dom7|ssr-window|swiper)\/).*/,
          use: [
            ...(dev ? [{ loader: 'cache-loader' }] : []),
            { loader: 'babel-loader' }
          ]
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            ...(dev
              ? [
                  { loader: 'cache-loader' },
                  {
                    loader: 'style-loader',
                    options: { sourceMap: dev }
                  }
                ]
              : [MiniCssExtractWebpackPlugin.loader]),
            { loader: 'css-loader', options: { sourceMap: dev } },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                sourceMap: dev,
                config: { ctx: { dev, pretty } }
              }
            },
            { loader: 'resolve-url-loader', options: { sourceMap: dev } },
            {
              loader: 'sass-loader',
              options: { sourceMap: true, sourceMapContents: dev }
            }
          ]
        },
        {
          test: /\.(ttf|otf|eot|woff2?)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'fonts/[name].[ext]'
              }
            }
          ]
        },
        {
          test: /\.(png|jpe?g|gif|svg|ico|webm)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192,
                name: 'img/[name].[ext]'
              }
            }
          ]
        }
      ]
    },
    plugins: [
      ...(dev
        ? [
            new webpack.HotModuleReplacementPlugin(),
            new FriendlyErrorsWebpackPlugin()
          ]
        : [
            new MiniCssExtractWebpackPlugin({
              filename: dev || pretty ? 'css/style.css' : 'css/style.min.css'
            }),
            new NonJsEntryCleanupPlugin({
              context: 'styles',
              extensions: 'js',
              includeSubfolders: true
            }),
            new CleanWebpackPlugin([path.resolve(outputFolder)], {
              allowExternal: true,
              beforeEmit: true
            }),
            new CopyWebpackPlugin(
              [
                {
                  from: path.resolve(`${context}/**/*`),
                  to: path.resolve(outputFolder)
                }
              ],
              {
                ignore: ['*.js', '*.ts', '*.sass', '*.scss', '*.css']
              }
            ),
            new ImageminPlugin({
              pngquant: { quality: '80-80' },
              plugins: [imageminMozjpeg({ quality: 73 })]
            })
          ])
    ]
  };
};
